﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Assignment7;
using System.Data.SqlClient;
using System.Data;

namespace Assignment7
{
    /// <summary>
    /// Interaction logic for ViewDepsEmpWindow.xaml
    /// </summary>
    public partial class ViewDepsEmpWindow : Window
    {
        SqlDataAdapter adapterDept;
        SqlDataAdapter adapterEmp;
        DataTable dtEmp;
        DataTable dtDept;

        public ViewDepsEmpWindow()
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            MyCompany.DBConnect.Connect();
        }

        private void CbDeps_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbDeps.SelectedValue != null)
            {
                SqlCommand command = new SqlCommand("SELECT id, dep_id, name, age, salary FROM Employee where dep_id = " + cbDeps.SelectedValue, MyCompany.DBConnect.connection);
                adapterEmp.SelectCommand = command;
                dtEmp = new DataTable();
                adapterEmp.Fill(dtEmp);
                dgEmp.DataContext = dtEmp.DefaultView;
            }
        }

        private void LvEmps_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //if (lvEmps.SelectedItem != null)
            //{
            //    EmpEditor editor = new EmpEditor
            //    {
            //        CurEmployee = lvEmps.SelectedItem as Employee,
            //        CurDept = cbDeps.SelectionBoxItem as Dept
            //    };
            //    editor.ShowDialog();
            //    if (editor.DialogResult.HasValue && editor.DialogResult.Value)
            //    {
            //        Refresh();
            //    }
            //}
        }

        private void Refresh()
        {
            // если изминили данные, надо обновить вид
            //ICollectionView view = CollectionViewSource.GetDefaultView(lvEmps.ItemsSource);
            //view.Refresh();
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            {
                adapterDept = new SqlDataAdapter();
                adapterEmp = new SqlDataAdapter();

                SqlCommand command = new SqlCommand("SELECT id, caption FROM Departament", MyCompany.DBConnect.connection);
                adapterDept.SelectCommand = command;
                dtDept = new DataTable();
                adapterDept.Fill(dtDept);
                cbDeps.ItemsSource = dtDept.DefaultView;
                cbDeps.SelectedIndex = 0;


                SqlParameter param;

                //insert
                command = new SqlCommand(@"INSERT INTO Departament (Caption) VALUES (@caption); SET @id = @@IDENTITY;", MyCompany.DBConnect.connection);
                command.Parameters.Add("@caption", SqlDbType.NVarChar, -1, "caption");
                param = command.Parameters.Add("@id", SqlDbType.Int, 0, "id");
                param.Direction = ParameterDirection.Output;
                adapterDept.InsertCommand = command;
                // update
                command = new SqlCommand(@"UPDATE Departament SET caption = @caption WHERE id = @id", MyCompany.DBConnect.connection);
                command.Parameters.Add("@caption", SqlDbType.NVarChar, -1, "caption");
                param = command.Parameters.Add("@id", SqlDbType.Int, 0, "id");
                param.SourceVersion = DataRowVersion.Original;
                adapterDept.UpdateCommand = command;
                //delete
                command = new SqlCommand("DELETE FROM Departament WHERE id = @id", MyCompany.DBConnect.connection);
                param = command.Parameters.Add("@id", SqlDbType.Int, 0, "id");
                param.SourceVersion = DataRowVersion.Original;
                adapterDept.DeleteCommand = command;


                //insert
                command = new SqlCommand(@"INSERT INTO Employee (dep_id, name, age, salary) VALUES (@dep_id, @name, @age, @salary); SET @id = @@IDENTITY;", MyCompany.DBConnect.connection);
                command.Parameters.Add("@dep_id", SqlDbType.Int, -1, "dep_id");
                command.Parameters.Add("@name", SqlDbType.NVarChar, -1, "name");
                command.Parameters.Add("@age", SqlDbType.Int, -1, "age");
                command.Parameters.Add("@salary", SqlDbType.Float, -1, "salary");
                param = command.Parameters.Add("@id", SqlDbType.Int, 0, "id");
                param.Direction = ParameterDirection.Output;
                adapterEmp.InsertCommand = command;
                // update
                command = new SqlCommand(@"UPDATE Employee SET dep_id = @dep_id, name = @name, age = @age, salary = @salary WHERE id = @id", MyCompany.DBConnect.connection);
                command.Parameters.Add("@dep_id", SqlDbType.Int, -1, "dep_id");
                command.Parameters.Add("@name", SqlDbType.NVarChar, -1, "name");
                command.Parameters.Add("@age", SqlDbType.Int, -1, "age");
                command.Parameters.Add("@salary", SqlDbType.Float, -1, "salary");
                param = command.Parameters.Add("@id", SqlDbType.Int, 0, "id");
                param.SourceVersion = DataRowVersion.Original;
                adapterEmp.UpdateCommand = command;
                //delete
                command = new SqlCommand("DELETE FROM Employee WHERE id = @id", MyCompany.DBConnect.connection);
                param = command.Parameters.Add("@id", SqlDbType.Int, 0, "id");
                param.SourceVersion = DataRowVersion.Original;
                adapterEmp.DeleteCommand = command;

            }
        }

        private void BtnAddEmp_OnClick(object sender, RoutedEventArgs e)
        {
            // добавим новую строку

            DataRow newRow = dtEmp.NewRow();
            EmpEditor editor = new EmpEditor(dtDept, newRow);
            editor.ShowDialog();
            if (editor.DialogResult.HasValue && editor.DialogResult.Value)
            {
                dtEmp.Rows.Add(newRow);
                adapterEmp.Update(dtEmp);
            }
        }

        private void btnUpdateEmp_Click(object sender, RoutedEventArgs e)
        {
            if (dgEmp.SelectedItem != null)
            {
                DataRowView newRow = (DataRowView)dgEmp.SelectedItem;
                newRow.BeginEdit();
                EmpEditor editor = new EmpEditor(dtDept, newRow.Row);
                editor.ShowDialog();
                if (editor.DialogResult.HasValue && editor.DialogResult.Value)
                {
                    newRow.EndEdit();
                    adapterEmp.Update(dtEmp);
                }
                else
                {
                    newRow.CancelEdit();
                }
            }
        }

        private void btnDeleteEmp_Click(object sender, RoutedEventArgs e)
        {
            if (dgEmp.SelectedItem != null)
            {
                DataRowView newRow = (DataRowView)dgEmp.SelectedItem;
                newRow.Row.Delete();
                adapterEmp.Update(dtEmp);
            }
        }

        private void btnDeptAdd_Click(object sender, RoutedEventArgs e)
        {
            // добавим новую строку

            DataRow newRow = dtDept.NewRow();
            InputBox inputBox = new InputBox();
            inputBox.ShowDialog();
            if (inputBox.DialogResult.HasValue && inputBox.DialogResult.Value)
            {
                newRow["caption"] = inputBox.tbCaption.Text;
                dtDept.Rows.Add(newRow);
                adapterDept.Update(dtDept);
            }

        }

        private void btnDeptEdit_Click(object sender, RoutedEventArgs e)
        {
            DataRowView newRow = (DataRowView)cbDeps.SelectedItem;
            newRow.BeginEdit();
            InputBox inputBox = new InputBox();
            inputBox.tbCaption.Text = newRow["caption"].ToString();
            inputBox.ShowDialog();
            if (inputBox.DialogResult.HasValue && inputBox.DialogResult.Value)
            {
                newRow["caption"] = inputBox.tbCaption.Text;
                newRow.EndEdit();
                adapterDept.Update(dtDept);
            }
            else
            {
                newRow.CancelEdit();
            }
        }

        private void btnDeptDelete_Click(object sender, RoutedEventArgs e)
        {
            DataRowView newRow = (DataRowView)cbDeps.SelectedItem;
            newRow.Row.Delete();
            adapterDept.Update(dtDept);
            if (cbDeps.Items.Count > 0)
                cbDeps.SelectedIndex = 0;
        }
    }
}
