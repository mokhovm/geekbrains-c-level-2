﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment7
{
    #region Assignment #7
    /*
    
    Домажнее задание #7.
    Выполнил Мохов Михаил
     
    Изменить WPF приложение для ведения списка сотрудников компании, из урока №5, используя
    связывание​​данных,​​DataGrid​​и​​ADO.NET​.
    1. Создать таблицы Employee и Department в БД MSSQL Server и заполните списки
    сущностей начальными данными.
    2. Для списка сотрудников и списка департаментов предусмотреть визуализацию
    (отображение). Это можно сделать, например, с использованием ComboBox или ListView.
    3. Предусмотреть возможность редактирования сотрудников и департаментов. Должна быть
    возможность изменить департамент у сотрудника. Список департаментов для выбора,
    можно выводить в ComboBox, это все можно выводить на дополнительной форме.
    4. Предусмотреть возможность создания новых сотрудников и департаментов. Реализовать
    данную возможность либо на форме редактирования, либо сделать новую форму.
    */
    #endregion


    public class MyCompany
    {

        private const string cs = @"Data Source = (localdb)\MSSQLLocalDB; Initial Catalog = Lesson7; Integrated Security = True";

        private static DBConnect _dbConnect;

        public static DBConnect DBConnect => _dbConnect ?? (_dbConnect = new DBConnect(cs));

    } 


}
