﻿drop table Employee;

drop table Departament;

CREATE TABLE [dbo].[Departament]
(
	[id] INT IDENTITY (1, 1) NOT NULL PRIMARY KEY, 
    [caption] NVARCHAR(MAX) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[description] NVARCHAR(MAX) COLLATE Cyrillic_General_CI_AS NULL
)

CREATE TABLE [dbo].[Employee] (
    [id]     INT            IDENTITY (1, 1) NOT NULL PRIMARY KEY,
    [dep_id] INT            NOT NULL,
    [name]   NVARCHAR (MAX) COLLATE Cyrillic_General_CI_AS NOT NULL,
    [age]    INT            NOT NULL,
    [salary] FLOAT (53)     NULL,
    CONSTRAINT [FK_DEP_EMP] FOREIGN KEY ([dep_id]) REFERENCES [dbo].[Departament] ([id]) ON DELETE CASCADE ON UPDATE CASCADE
);

delete Employee;

delete Departament;

insert into Departament (caption)
values (N'Бухгалтерия' );

insert into Departament (caption)
values (N'Плановый отдел' );

insert into Departament (caption)
values (N'Администрация' );

insert into Departament (caption)
values (N'Служба безопасности' );

insert into Departament (caption)
values (N'Отдел продаж' );

insert into Departament (caption)
values (N'Производство' );


insert into Employee (dep_id, name, age, salary)
values (1, N'Федер Бондарчук', 40, 200 );

insert into Employee (dep_id, name, age, salary)
values (1, N'Алиса Фрейндлих', 70, 1000 );

insert into Employee (dep_id, name, age, salary)
values (1, N'Сергей Чоношвилли', 50, 500 );

insert into Employee (dep_id, name, age, salary)
values (2, N'Сергей Добронравов', 25, 600 );

insert into Employee (dep_id, name, age, salary)
values (2, N'Егор Крид', 90, 500 );

insert into Employee (dep_id, name, age, salary)
values (2, N'Диана Гурцкая', 35, 350 );

insert into Employee (dep_id, name, age, salary)
values (3, N'Антон Городецкий', 36, 550 );

insert into Employee (dep_id, name, age, salary)
values (3, N'Иван Ургант', 16, 400 );

insert into Employee (dep_id, name, age, salary)
values (3, N'Петр Толстой', 28, 350 );

insert into Employee (dep_id, name, age, salary)
values (4, N'Ума Турман', 40, 700 );

insert into Employee (dep_id, name, age, salary)
values (4, N'Иннокентий Смоктуновский', 55, 200 );

insert into Employee (dep_id, name, age, salary)
values (4, N'Александр Карелин', 30, 250 );

insert into Employee (dep_id, name, age, salary)
values (5, N'Шерлок Холмс', 221, 100 );

insert into Employee (dep_id, name, age, salary)
values (5, N'Иван Демидов', 46, 400 );

insert into Employee (dep_id, name, age, salary)
values (5, N'Александр Третьяк', 55, 450 );

insert into Employee (dep_id, name, age, salary)
values (6, N'Сергей Капица', 80, 330 );

insert into Employee (dep_id, name, age, salary)
values (6, N'Эмир Кустурица', 33, 360 );

insert into Employee (dep_id, name, age, salary)
values (6, N'Валентина Толкунова', 20, 90 );
