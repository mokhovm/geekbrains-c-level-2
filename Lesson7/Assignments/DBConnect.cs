﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment7
{
    public class DBConnect
    {
        private string connectionString;
        public SqlConnection connection;



        public DBConnect(string connectionString)
        {
            this.connectionString = connectionString;
        }

        ~DBConnect()
        {
            //connection.Close();
            connection = null;
        }


        public void Connect ()
        {
            connection = new SqlConnection(connectionString);
            connection.Open();
            string sqlExpression = "SELECT COUNT(*) FROM Employee";

            SqlCommand command = new SqlCommand(sqlExpression, connection);
            int cnt = Convert.ToInt32(command.ExecuteScalar());
            System.Diagnostics.Debug.WriteLine("количество:" + cnt);
        }
    }
}
