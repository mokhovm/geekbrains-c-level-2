﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Assignment7
{
    /// <summary>
    /// Interaction logic for EmpEditor.xaml
    /// </summary>
    public partial class EmpEditor : Window
    {

        private DataTable dtDept;
        public DataRow dataRow;

        public EmpEditor(DataTable dtDept, DataRow dataRow)
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            Loaded += MyWindow_Loaded;
            this.dtDept = dtDept;
            this.dataRow = dataRow;
        }

        private void MyWindow_Loaded(object sender, RoutedEventArgs e)
        {
            cbDept.ItemsSource = dtDept.DefaultView;
            tbName.Text = dataRow["name"].ToString();
            tbAge.Text = dataRow["age"].ToString();
            tbSalary.Text = dataRow["Salary"].ToString();
            if (int.TryParse(dataRow["dep_id"].ToString(),  out int id))
                cbDept.SelectedValue = dataRow["dep_id"] = id;
        }


        private void BtnApply_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            dataRow["name"] = tbName.Text;
            dataRow["age"] = tbAge.Text;
            dataRow["Salary"] = tbSalary.Text;
            dataRow["dep_id"] = cbDept.SelectedValue;
            Close();
        }

        private void BtnCancel_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

    }
}
