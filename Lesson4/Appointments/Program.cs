﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Appointments
{
    #region Appointment

    /*
    2. Дана коллекция List<T>, требуется подсчитать, сколько раз каждый элемент встречается в
    данной коллекции.
    а) для целых чисел;
    б) *для обобщенной коллекции;
    в)**используя Linq.
    

    3. *Дан фрагмент программы:
    Dictionary<string, int> dict = new Dictionary<string, int>()
    {
    {"four",4 },
    {"two",2 },
    { "one",1 },
    {"three",3 },
    };
    var d = dict.OrderBy(delegate(KeyValuePair<string,int> pair) { return pair.Value; });
    foreach (var pair in d)
    {
    Console.WriteLine("{0} - {1}", pair.Key, pair.Value);
    }
    а) Свернуть обращение к OrderBy с использованием лямбда-выражения$
    б) *Развернуть обращение к OrderBy с использованием делегата Func<KeyValuePair<string, int>, int>
    */
    #endregion

    class Program
    {
        public static List<int> list1 = new List<int> {1,1,2,3,3,3,3,3,4,4,4,5,6,6,7,9,9,9};
        public static List<string> list2 = new List<string> { "А", "А", "А", "Б", "Б", "В", "В", "Ю"};

        private static Dictionary<int, int> Method1(List<int> aList)
        {
            Console.WriteLine("Подсчитаем, сколько раз встречается каждый элемент для коллекции из целых чисел");
            var res = new Dictionary<int, int>();
            foreach (var item in aList)
            {
                if (res.ContainsKey(item))
                    res[item]++;
                else
                    res.Add(item, 1);
            }
            return res;
        }

        private static Dictionary<T, int> Method2<T>(List<T> aList)
        {
            Console.WriteLine("Подсчитаем, сколько раз встречается каждый элемент для обобщенной коллекции");
            var res = new Dictionary<T, int>();
            foreach (T item in aList)
            {
                if (res.ContainsKey(item))
                    res[item]++;
                else
                    res.Add(item, 1);
            }
            return res;
        }

        
        private static void Method3<T>(List<T> aList)
        {
            Console.WriteLine("Подсчитаем, сколько раз встречается каждый элемент для обобщенной коллекции с помощью linq");
            var answer = aList.GroupBy(l => l)
                .Select(data => new {key = data.Key, qty = data.Select(l => l).Count()});

            foreach (var item in answer)
            {
                Console.WriteLine($"Значение {item.key} встречается {item.qty} раз(а)");
            }

        }
        

        private static void ShowData<T>(Dictionary<T, int> dict)
        {
            foreach (var item in dict.Keys)
            {
                Console.WriteLine($"Значение {item} встречается {dict[item]} раз(а)");
            }
            Console.WriteLine();
        }



        /// <summary>
        /// Тестируем работу с делегатами
        /// </summary>
        private static void testOrderBy()
        {

            Console.WriteLine("Тестируем работу с делегатами: \n");

            void ShowData(String caption, IOrderedEnumerable<KeyValuePair<string, int>> list)
            {
                Console.WriteLine(caption);
                foreach (var pair in list)
                {
                    Console.WriteLine("{0} - {1}", pair.Key, pair.Value);
                }
                Console.WriteLine();
            }

            // Исходный словарь
            Dictionary<string, int> dict = new Dictionary<string, int>()
            {
                {"four",4 },
                {"two",2 },
                { "one",1 },
                {"three",3 },
            };

            // Исходное решение
            var d1 = dict.OrderBy(delegate (KeyValuePair<string, int> pair) { return pair.Value; });
            ShowData("Исходное решение", d1);

            // Решение через лямбда-выражения
            var d2 = dict.OrderBy(pair => pair.Value);
            ShowData("Решение через лямбда-выражения", d2);

            // Решение с помощью Func
            Func<KeyValuePair<String, int>, int> myFunc = delegate(KeyValuePair<String, int> pair)
            {
                return pair.Value;
            };

            var d3 = dict.OrderBy(myFunc);
            ShowData("Решение с помощью Func", d3);
        }

        static void Main(string[] args)
        {
            ShowData(Method1(list1));
            ShowData(Method2(list1));
            Method3(list1);
            testOrderBy();
            Console.ReadKey();
        }
    }
}
