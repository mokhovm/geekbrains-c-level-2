﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
    /// <summary>
    /// Сотрудник с фиксированной зарплатой
    /// </summary>
    class FixedPaymentEmploye : CustomEmploye
    {
        private double salary;

        public FixedPaymentEmploye(string name, double salary) : base (name)
        {
            this.salary = salary;
        }

        public override double CalcSalary()
        {
            //«среднемесячная заработная плата = фиксированной месячной оплате».
            return salary;
        }

    }
}
