﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
    /// <summary>
    /// Класс Департамент, реализующий внутри список сотрудников. 
    /// Класс поддерживает интерефейс IEnumerable и индексатор
    /// </summary>
    class Dept : IEnumerable

    {
        private CustomEmploye[] list;
        private int curPos;

        public int Length => list.Length;

        public Dept(int length)
        {
            list = new CustomEmploye[length];
        }

        public void Sort()
        {
            Array.Sort(list);
        }

        /// <summary>
        /// индексатор для доступа к элементам массива
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public CustomEmploye this[int index]
        {
            get => list[index];
            set => list[index] = value;
        }

        /// <summary>
        /// Воспользуемся стандартным енумератором массива
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return list.GetEnumerator();
        }
        
    }
}
