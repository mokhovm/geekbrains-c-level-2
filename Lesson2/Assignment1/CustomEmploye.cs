﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
    /// <summary>
    /// Базовый класс работника
    /// </summary>
    abstract class CustomEmploye:IComparable<CustomEmploye>
    {
        public string Name { get; set; }

        public CustomEmploye(string name)
        {
            this.Name = name;
        }

        /// <summary>
        /// Расчет зарплаты будет реализован в потомках
        /// </summary>
        /// <returns></returns>
        public abstract double CalcSalary();

        /// <summary>
        /// Метод для сравнения зарплат. Реализует интрефейс IComparable
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(CustomEmploye other)
        {
            int res;
            if (other.CalcSalary() > CalcSalary())
                res = -1;
            else if (other.CalcSalary() < CalcSalary())
                res = 1;
            else
                res = 0;
            return res;
        }

        public override string ToString()
        {
            return $"{Name} {CalcSalary()}";
        }
    }

 
}
