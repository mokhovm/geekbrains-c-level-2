﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
    #region Assignment #1

    /*
    Выполнил Мохов Михаил

    Построить три класса(базовый и 2 потомка), описывающих некоторых работников с
    почасовой оплатой(один из потомков) и фиксированной оплатой(второй потомок).
    а) Описать в базовом классе абстрактный метод для расчёта среднемесячной заработной
    платы.Для «повременщиков» формула для расчета такова: «среднемесячная заработная
    плата = 20.8 * 8 * почасовую ставку», для работников с фиксированной оплатой
    «среднемесячная заработная плата = фиксированной месячной оплате».
    б) Создать на базе абстрактного класса массив сотрудников и заполнить его.
    в) ** Реализовать интерфейсы для возможности сортировки массива используя Array.Sort().
    г) *** Создать класс, содержащий массив сотрудников и реализовать возможность вывода
    данных с использованием foreach.

    */

    #endregion
    class Program
    {
        // размер массива с сотрудниками
        private const int DEPT_LEN = 10;
        // счетчик сотрудников
        private static int empQty = 0;
        private static Random rnd;

        /// <summary>
        /// Возвращает нового случайного сотрудника
        /// </summary>
        /// <returns></returns>
        private static CustomEmploye InitNewEmployee()
        {
            CustomEmploye res;
            empQty++;
            string name = "Employe" + empQty.ToString();
            if (rnd.Next(2) == 0)
                res = new HourlyEmploye(name, rnd.Next(10, 20));
            else
                res = new FixedPaymentEmploye(name, rnd.Next(2000, 3000));
            return res;
        }


        /// <summary>
        /// Выводит на экран содержимое списка
        /// </summary>
        /// <param name="list"></param>
        /// <param name="note"></param>
        public static void PrintEmployes(IEnumerable list, String note)
        {
            Console.WriteLine($"{note}");
            foreach (CustomEmploye e in list)
            {
                Console.WriteLine(e);
            }
            Console.WriteLine();
        }

        private static void TestArray()
        {
            CustomEmploye[] emps;
            Console.WriteLine("Тестируем массив");
            emps = new CustomEmploye[DEPT_LEN];
            for (int i = 0; i < emps.Length; i++)
                emps[i] = InitNewEmployee();

            PrintEmployes(emps, "Список сотрудников до сортировки:");
            Array.Sort(emps);
            PrintEmployes(emps, "Список сотрудников после сортировки по зпрплате:");
        }

        private static void TestClass()
        {
            Console.WriteLine("Тестируем класс с итератором");
            Dept dept = new Dept(DEPT_LEN);
            for (int i = 0; i < dept.Length; i++)
                dept[i] = InitNewEmployee();
            PrintEmployes(dept, "Список сотрудников до сортировки:");
            dept.Sort();
            PrintEmployes(dept, "Список сотрудников после сортировки по зпрплате:");
        }

        public static void Main(string[] args)
        {
            rnd = new Random();
            TestArray();
            TestClass();
            Console.ReadKey();
        }
    }
}
