﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
    /// <summary>
    /// сотрудник с почасовой зарплатой
    /// </summary>
    class HourlyEmploye : CustomEmploye
    {
        public double HourlyRate { get; set; }

        public HourlyEmploye(string name, double hourlyRare) : base (name)
        {
            this.HourlyRate = hourlyRare;
        }

        public override double CalcSalary()
        {
            //«среднемесячная заработная плата = 20.8 * 8 * почасовую ставку»
            return HourlyRate * 20.8 * 8;
        }
 
    }
}
