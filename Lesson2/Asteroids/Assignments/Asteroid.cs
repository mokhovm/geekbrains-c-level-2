﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignments
{
    class Asteroid : BaseObject
    {
        public int Power { get; set; }
        
        public Asteroid(Point pos, Point dir, Size size) : base(pos, dir, size)
        {
        }

        protected override void Init()
        {
        }

        public override void Draw()
        {
            Game.buffer.Graphics.FillEllipse(Brushes.White, pos.X, pos.Y, size.Width, size.Height);
        }

        public override void Update()
        {
            pos.X += dir.X;
            pos.Y += dir.Y;

            if (pos.X + size.Width < 0)
            {
                pos.X = Game.Width;
                pos.Y = new Random().Next(Game.Height);
                dir.Y = -dir.Y;
            }
        }
    }
}
