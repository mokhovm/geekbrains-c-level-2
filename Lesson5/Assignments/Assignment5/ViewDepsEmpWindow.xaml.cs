﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Assignment5
{
    /// <summary>
    /// Interaction logic for ViewDepsEmpWindow.xaml
    /// </summary>
    public partial class ViewDepsEmpWindow : Window
    {
        public ViewDepsEmpWindow()
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            cbDeps.ItemsSource = MyCompany.Company.Depts;
            cbDeps.SelectedIndex = 0;
        }

        private void CbDeps_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //MessageBox.Show(e.AddedItems[0].ToString());
            var dept = e.AddedItems[0] as Dept;
            lvEmps.ItemsSource = dept.Employees;
        }

        private void LvEmps_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (lvEmps.SelectedItem != null)
            {
                EmpEditor editor = new EmpEditor
                {
                    CurEmployee = lvEmps.SelectedItem as Employee,
                    CurDept = cbDeps.SelectionBoxItem as Dept
                };
                editor.ShowDialog();
                if (editor.DialogResult.HasValue && editor.DialogResult.Value)
                {
                    Refresh();
                }
            }
        }

        private void Refresh()
        {
            // если изминили данные, надо обновить вид
            ICollectionView view = CollectionViewSource.GetDefaultView(lvEmps.ItemsSource);
            view.Refresh();
        }

        private void BtnAdd_OnClick(object sender, RoutedEventArgs e)
        {
            EmpEditor editor = new EmpEditor();
            editor.ShowDialog();
            if (editor.DialogResult.HasValue && editor.DialogResult.Value)
            {
                Refresh();
            }
        }
    }
}
