﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignments
{
    #region Level 2 Assignment #1
    /*
        Выполнил Мохов Михаил.
        
        Добавить свои объекты в иерархию объектов, чтобы получился красивый задний фон,
        похожий на полёт в звёздном пространстве.
        *Заменить кружочки картинками, используя метод DrawImage.
    */
    #endregion


    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            /*
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
            */

            Form form = new Form();
            form.Width = Game.DEF_WIDTH;
            form.Height = Game.DEF_HEIGHT;
            form.StartPosition = FormStartPosition.CenterScreen;
            Game.Init(form);
            Game.Draw();

            form.Show();
            Application.Run(form);
        }
    }
}
