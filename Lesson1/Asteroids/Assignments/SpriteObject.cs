﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignments
{
    /// <summary>
    /// Класс для работы со спрайтами
    /// </summary>
    class SpriteObject :BaseObject
    {
        /// <summary>
        /// изображение
        /// </summary>
        protected Image img;
        /// <summary>
        /// прямоугольник, в которое вписано изображение
        /// требуется для работы с масштабированием изображения
        /// </summary>
        protected Rectangle rect;

        public SpriteObject(Point pos, Point dir, Size size, string fileName) : base(pos, dir, size)
        {
            img = Image.FromFile(fileName);
            // если указаны размеры изображения, то оно будет отмасштабировано до этих размеров
            // при выводе в буффер. В противном случае будут использоваться начальные размеры изображения
            if (size.Width == 0) this.size.Height = img.Height;
            if (size.Height == 0) this.size.Width = img.Width;
        }

        /// <summary>
        /// рисуем изображение в буфере игры
        /// </summary>
        public override void Draw()
        {
            Game.buffer.Graphics.DrawImage(img, rect);
        }

        /// <summary>
        /// обновляем координаты объекта
        /// </summary>
        public override void Update()
        {
            base.Update();
            rect.X = pos.X;
            rect.Y = pos.Y;
            rect.Width = size.Width;
            rect.Height = size.Height;
        }

        /// <summary>
        /// Случайным образом перевернет картинку
        /// </summary>
        public void RndRotare()
        {
            Array values = Enum.GetValues(typeof(RotateFlipType));
            Random random = new Random();
            RotateFlipType rotare = (RotateFlipType)values.GetValue(random.Next(values.Length));
            img.RotateFlip(rotare);

        }
    }
}
