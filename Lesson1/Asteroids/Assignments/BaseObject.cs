﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignments
{
    /// <summary>
    /// Базовый класс для объектов игры
    /// </summary>
    class BaseObject
    {
        protected Point pos;
        protected Point dir;
        protected Size size;

        public BaseObject(Point pos, Point dir, Size size)
        {
            this.pos = pos;
            this.dir = dir;
            this.size = size;
        }

        public virtual void Draw()
        {
            throw new NotImplementedException();
        }

        public virtual void Update()
        {
            pos.X += dir.X;
            pos.Y += dir.Y;

            if (pos.X + size.Width < 0) pos.X = Game.Width;
        }

    }
}
