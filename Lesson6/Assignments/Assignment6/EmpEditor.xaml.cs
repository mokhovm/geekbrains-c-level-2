﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Assignment6
{
    /// <summary>
    /// Interaction logic for EmpEditor.xaml
    /// </summary>
    public partial class EmpEditor : Window
    {
        private Dept _prevDept;
        private Dept _dept;
        public Employee CurEmployee { get; set; }
        public Dept CurDept {
            get { return _dept; }
            set { _prevDept = value;
                _dept = value;
            }
        }

        public EmpEditor()
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            Loaded += MyWindow_Loaded;
        }

        private void MyWindow_Loaded(object sender, RoutedEventArgs e)
        {
            cbDept.ItemsSource = MyCompany.Company.Depts;
            if (CurEmployee != null)
            { 
                tbAge.Text = CurEmployee.Age.ToString();
                tbName.Text = CurEmployee.Name;
                tbSalary.Text = CurEmployee.Salary.ToString();
                int index = cbDept.Items.IndexOf(CurDept);
                cbDept.SelectedIndex = index;
            }
        }


        private void BtnApply_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            if (CurEmployee == null)
            {
                CurEmployee = new Employee();
            }
            CurEmployee.Name = tbName.Text;
            CurEmployee.Age = Convert.ToInt32(tbAge.Text);
            CurEmployee.Salary = Convert.ToDouble(tbSalary.Text);
            _dept = cbDept.SelectedItem as Dept;
            if (_prevDept != _dept)
            {
                _prevDept?.Employees.Remove(CurEmployee);
                _dept.Employees.Add(CurEmployee);
            }
            Close();
        }

        private void BtnCancel_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

    }
}
