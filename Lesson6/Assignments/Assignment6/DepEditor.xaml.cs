﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Assignment6;

namespace Assignment6
{
    /// <summary>
    /// Interaction logic for DepEditor.xaml
    /// </summary>
    public partial class DepEditor : Window
    {
        public DepEditor()
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            Loaded += MyWindow_Loaded;
        }

        private void MyWindow_Loaded(object sender, RoutedEventArgs e)
        {
            lvDeps.ItemsSource = MyCompany.Company.Depts;
            lvDeps.SelectedIndex = 0;
        }

        private void LvDeps_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        private void BtnAdd_OnClick(object sender, RoutedEventArgs e)
        {
            InputBox input = new InputBox();
            input.ShowDialog();
            if (input.DialogResult.HasValue && input.DialogResult.Value)
            {
                MyCompany.Company.Depts.Add(new Dept(input.tbCaption.Text, 0));
                //Refresh();
            }

        }

        private void Refresh()
        {
            // если изминили данные, надо обновить вид
            ICollectionView view = CollectionViewSource.GetDefaultView(lvDeps.ItemsSource);
            view.Refresh();
        }
    }
}
