﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment6
{
    #region Assignment #6
    /*
    
    Домажнее задание #6.
    Выполнил Мохов Михаил
     
    Изменить WPF приложение для ведения списка сотрудников компании, из урока №5, используя
    связывание​​данных,​​ ListView, ​​ObservableCollection ​​и​ ​INotifyPropertyChanged​.
    1. Создать сущности Employee и Department и заполните списки сущностей начальными
    данными.
    2. Для списка сотрудников и списка департаментов предусмотреть визуализацию
    (отображение). Это можно сделать, например, с использованием ComboBox или ListView.
    3. Предусмотреть возможность редактирования сотрудников и департаментов. Должна быть
    возможность изменить департамент у сотрудника. Список департаментов для выбора,
    можно выводить в ComboBox, это все можно выводить на дополнительной форме.
    4. Предусмотреть возможность создания новых сотрудников и департаментов. Реализовать
    данную возможность либо на форме редактирования, либо сделать новую форму.    */
    #endregion

    /// <summary>
    /// Описывает служащего
    /// </summary>
    public class Employee
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public double Salary { get; set; }

        public Employee(string name, int age, double salary)
        {
            Name = name;
            Age = age;
            Salary = salary;
        }

        public Employee()
        {
            Id = new Random().Next(1, Int32.MaxValue);
        }

        public override string ToString()
        {
            return $"{Id}\t{Name}\t{Age}\t{Salary}";
        }
    }

    /// <summary>
    /// Опсывает подразделение
    /// </summary>
    public class Dept
    {
        public const int MAX_EMP = 10;

        public string Caption { get; set; }
        public ObservableCollection<Employee> Employees;

        public Dept(string aCaption, int empQty = 0)
        {
            Caption = aCaption;
            Random rnd = new Random();
            Employees = new ObservableCollection<Employee>();
            for(var i = 0; i < empQty; i++)
            {
                Employees.Add(new Employee($"Employee_{i.ToString()}", rnd.Next(18, 70), rnd.Next(100, 900)));
            }
        }

        public override string ToString()
        {
            return Caption;
        }
    }

    public class TheCompany
    {
        public string Caption { get; set; }
        public ObservableCollection<Dept> Depts;

        public TheCompany(string aCaption)
        {
            Caption = aCaption;
            Depts = new ObservableCollection<Dept>();
            Depts.Add(new Dept("Бухгалтерия", 10));
            Depts.Add(new Dept("Тех. поддержка", 1));
            Depts.Add(new Dept("Руководство", 5));
            Depts.Add(new Dept("Маркетинг", 7));
            Depts.Add(new Dept("Экономический отдел", 6));
            Depts.Add(new Dept("Производство", 1));
        }
    }

    public class MyCompany
    {
        private static TheCompany _company;

        public static TheCompany Company => _company ?? (_company = new TheCompany("Рога и копыта"));

    } 


}
