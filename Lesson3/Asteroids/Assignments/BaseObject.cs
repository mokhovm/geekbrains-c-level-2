﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignments
{
    /// <summary>
    /// Базовый класс для объектов игры
    /// </summary>
    abstract class BaseObject : ICollision
    {

        protected Point pos;
        protected Point dir;
        protected Size size;

        public Rectangle Rect => new Rectangle(pos, size);

        public BaseObject(Point pos, Point dir, Size size)
        {
            this.pos = pos;
            this.dir = dir;
            this.size = size;
        }

        protected abstract void Init();

        public abstract void Draw();

        public abstract void Update();

        public void UpdateCoordX(int x)
        {
            pos.X = x;
        }

        public void UpdateCoordY(int y)
        {
            pos.Y = y;
        }

        public bool Collision(ICollision obj)
        {
            bool res = (obj.Rect.IntersectsWith(Rect));
            return res;
        }

        
    }
}
