﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignments
{

    interface ICollision
    {
        bool Collision(ICollision obj);
        Rectangle Rect { get; }
    }

    class Bullet : BaseObject
    {
        public Bullet(Point pos, Point dir, Size size) : base(pos, dir, size)
        {
        }

        protected override void Init()
        {
            throw new NotImplementedException();
        }

        public override void Draw()
        {
            Game.buffer.Graphics.DrawRectangle(Pens.OrangeRed, pos.X, pos.Y, size.Width, size.Height);
        }

        public override void Update()
        {
            pos.X = pos.X + dir.X;
            if (pos.X > Game.Width)
            {
                pos.X = 0;
            }
        }
    }
}
