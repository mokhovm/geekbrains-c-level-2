﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Assignments
{
    /// <summary>
    /// Реализует визуализацию и управление игрой
    /// </summary>
    class Game
    {
        public const int DEF_WIDTH = 800;
        public const int DEF_HEIGHT = 600;

        public const int MAX_SMALL_STARS = 30;
        public const int MAX_BIG_STARS = 10;

        private const string ASSET_SUN = @"images\sun.png";
        private const string ASSET_MOON = @"images\fobos.png";
        private const string ASSET_STAR = @"images\star.png";
        private const int MAX_SCREEN_SIZE = 1000;
        private const int MAX_ASTEROIDS = 3;

        //private const string ASSET_BTN_START = @"images\btnStart.png";
        //private const string ASSET_BTN_SCORE = @"images\btnScores.png";
        //private const string ASSET_BTN_EXIT = @"images\btnExit.png";


        private static Random rnd;
        // ссылка на солнце
        public static SpriteObject sun;
        // ссылка на луну
        public static SpriteObject moon;
        // ссылка на пулю
        public static BaseObject bullet;


        private static Form form;
        private static BufferedGraphicsContext context;
        public static BufferedGraphics buffer;
        public static int Width { get; set; }
        public static int Height { get; set; }
        public static List<BaseObject> objList;
        private static Timer timer;
        

        static Game()
        {
            
        }

        /// <summary>
        /// Инициализации параметров игры
        /// </summary>
        /// <param name="form"></param>
        public static void Init(Form form)
        {
            rnd = new Random();
            Game.form = form;
            objList = new List<BaseObject>();
            context = BufferedGraphicsManager.Current;
            Graphics g = form.CreateGraphics();
            buffer = context.Allocate(g, new Rectangle(0, 0, Width, Height));
            InitParallax();
            InitAsteroids();
            InitBullet();
            AddControls();
            InitGameTimer();
        }

        private static void InitGameTimer()
        {
            timer = new Timer {Interval = 50};
            timer.Start();
            timer.Tick += OnTimerTick;
        }

        /// <summary>
        /// Отсчитывает локальное время модели
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void OnTimerTick(object sender, EventArgs e)
        {
            Draw();
            Update();
        }

        /// <summary>
        /// прорисовка объектов в новой позиции
        /// </summary>
        public static void Draw()
        {
            buffer.Graphics.Clear(Color.Black);
            foreach (BaseObject obj in objList)
                obj.Draw();
            buffer.Render();
        }

        /// <summary>
        /// Обновление координат объектов модели
        /// </summary>
        public static void Update()
        {
            foreach (BaseObject obj in objList)
            {
                obj.Update();
                if (obj is Asteroid)
                {
                    if (obj.Collision(bullet))
                    {
                        DoHit(bullet, obj);
                    }
                }
            }

        }

        public static void DoHit(BaseObject blt, BaseObject asteroid)
        {
            System.Media.SystemSounds.Hand.Play();
            System.Diagnostics.Debug.WriteLine("bullet hit");

            blt.UpdateCoordX(0);
            asteroid.UpdateCoordY(Width);
            /*
            objList.Remove(asteroid);
            InitBullet();
            InitAsteroid();
            */
        }

        public static void InitAsteroid()
        {
            const int MIN_SIZE = 25;
            const int MAX_SIZE = 50;
            const int MAX_SPEED = 5;

           
         
            int initSize = rnd.Next(MIN_SIZE, MAX_SIZE);
            Size size = new Size(initSize, initSize);
            int speedX = -MAX_SPEED;
            int speedY = 0;
            objList.Add(new Asteroid(new Point(rnd.Next(0, Width), rnd.Next(0, Height)),
                new Point(speedX, speedY), size));

        }

        public static void InitAsteroids()
        {
            for (int i = 0; i < MAX_ASTEROIDS; i++)
            {
                InitAsteroid();
            }
        }

        public static void InitBullet()
        {
            bullet = new Bullet(new Point(0, 200), new Point(5, 0), new Size(4, 1));
            objList.Add(bullet);
        }


        /// <summary>
        /// Саздадим объекты для параллакса
        /// </summary>
        public static void InitParallax()
        {
            const int MAX_SIZE = 4;
            const int STAR_SCALE_MIN = 20;
            const int STAR_SCALE_MAX = 35;


            for (int i = 0; i < MAX_SMALL_STARS; i++)
            {
                int initSize = rnd.Next(1, MAX_SIZE);
                Size size = new Size(initSize, initSize);
                int speed = initSize * MAX_SIZE;
                objList.Add(new Star(new Point(rnd.Next(0, Width), i * Height / MAX_SMALL_STARS),
                    new Point(speed, 0), size));
            }

            for (int i = 0; i < MAX_BIG_STARS; i++)
            {
                int initSize = rnd.Next(STAR_SCALE_MIN, STAR_SCALE_MAX);
                Size size = new Size(initSize, initSize);
                int speed = -1;
                SpriteObject imgObj = new SpriteObject(new Point(rnd.Next(0, Height), i * Height / MAX_BIG_STARS),
                    new Point(speed, 0), size, ASSET_STAR);
                imgObj.RndRotare();
                objList.Add(imgObj);
            }

            sun = new SpriteObject(new Point(10, 10), new Point(-2, 0), new Size(0, 0), ASSET_SUN);
            moon = new SpriteObject(new Point(300, 300), new Point(-3, 0), new Size(0, 0), ASSET_MOON);
            objList.Add(sun);
            objList.Add(moon);

            // тестируем создание объекта с неуказанным именем файла
            try
            {
                moon = new SpriteObject(new Point(300, 300), new Point(-3, 0), new Size(0, 0), "");
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            

        }

        private static void AddControls()
        {
            var button1 = new System.Windows.Forms.Button
            {
                Location = new System.Drawing.Point(100, Height - 200),
                Name = "btnStart",
                Size = new System.Drawing.Size(75, 23),
                TabIndex = 0,
                Text = "Start",
                UseVisualStyleBackColor = true
            };
            button1.Click += onClick;
            form.Controls.Add(button1);

            button1 = new System.Windows.Forms.Button
            {
                Location = new System.Drawing.Point(350, Height - 200),
                Name = "btnScores",
                Size = new System.Drawing.Size(75, 23),
                TabIndex = 0,
                Text = "Scores",
                UseVisualStyleBackColor = true
            };
            button1.Click += onClick;
            form.Controls.Add(button1);

            button1 = new System.Windows.Forms.Button
            {
                Location = new System.Drawing.Point(600, Height - 200),
                Name = "btnExit",
                Size = new System.Drawing.Size(75, 23),
                TabIndex = 0,
                Text = "Exit",
                UseVisualStyleBackColor = true
            };
            button1.Click += onClick;
            form.Controls.Add(button1);

            Label label1 = new System.Windows.Forms.Label
            {
                AutoSize = true,
                Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204))),
                ForeColor = System.Drawing.SystemColors.ActiveCaption,
                BackColor = Color.Black,
                Location = new System.Drawing.Point(220, Height - 100),
                Name = "label1",
                Size = new System.Drawing.Size(268, 95),
                TabIndex = 1,
                Text = "Автор Мохов Михаил"
            };
            form.Controls.Add(label1);
        }

        private static void onClick(object sender, EventArgs e)
        {
            switch ((sender as Button).Name)
            {
                case "btnScores":
                {
                    MessageBox.Show("Таблица рекордов"); 
                }
                break;
                case "btnStart" :
                {
                    MessageBox.Show("Старт игры");
                }
                break;
                case "btnExit":
                {
                    Application.Exit();
                }
                    break;
            }

        }

        public static void SetScreenSize(Form form, int width, int height)
        {
            string param = String.Empty;
            if (width > MAX_SCREEN_SIZE || width < 0)
            {
                param = "Ширина";
            }

            if (height > MAX_SCREEN_SIZE || height < 0)
            {
                param = "Высота";
            }
            
            if (param != "")
            {
                throw new ArgumentOutOfRangeException(param);
            }
            else
            {
                form.Height = height;
                form.Width = width;
                Width = form.Width;
                Height = form.Height;
            }
                
        }
    }
}
